﻿using React_Boiler_Plate.Models;
using System.Linq;

namespace React_Boiler_Plate.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly BoilerPlateContext _context;

        public UserRepository(BoilerPlateContext context)
        {
            _context = context;
        }

        public User Create(User user)
        {
            _context.Users.Add(user);
            user.Id = _context.SaveChanges();

            return user;
        }

        public User GetByEmail(string email)
        {
            return _context.Users.FirstOrDefault(u => u.Email == email);
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }
    }
}
