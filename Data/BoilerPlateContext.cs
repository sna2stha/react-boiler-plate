﻿using Microsoft.EntityFrameworkCore;
using React_Boiler_Plate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace React_Boiler_Plate.Data
{
    public class BoilerPlateContext : DbContext
    {
        public BoilerPlateContext(DbContextOptions<BoilerPlateContext> options)
            : base(options)
        {

        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Name).HasColumnType("varchar(100)").IsRequired();
                entity.Property(e => e.Password).HasColumnType("varchar(256)").IsRequired();
                entity.HasIndex(e => e.Email).IsUnique();
            });
        }
    }
}
