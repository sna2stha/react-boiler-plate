﻿using React_Boiler_Plate.Models;

namespace React_Boiler_Plate.Data
{
    public interface IUserRepository
    {
        User Create(User user);
        User GetByEmail(string email);
        User GetById(int id);
    }
}
